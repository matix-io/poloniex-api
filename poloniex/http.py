import hashlib
import hmac
import json
import time
import requests
import re
from responses import RequestsMock
from urllib.parse import parse_qs
from urllib.parse import urlencode
from unittest.mock import Mock
# wat


class PoloniexInvalidCurrencyPair(Exception):
    pass


class PoloniexInvalidQuantity(Exception):
    pass


class PoloniexUnableToFill(Exception):
    pass


class PoloniexUnknown(Exception):
    pass


class PoloniexOrderNotFound(Exception):
    pass


class PoloniexPermissionDenied(Exception):
    pass


class PoloniexRateLimiting(Exception):
    pass


class PoloniexFailedToParseResponse(Exception):
    pass


class Poloniex:
    """
    Wrapper around the poloniex HTTP API
    """
    def __init__(self, KEY, SECRET, **kwargs):
        shot_timeout = kwargs.get('shot_timeout')
        self._key = KEY
        self._secret = SECRET.encode('utf-8')
        self._base = 'https://poloniex.com'

    def _build_url(self, path):
        return "{}{}".format(self._base, path)

    def _check_errors(self, res_json):
        if 'error' in res_json:
            err = res_json['error']
            if err[:23] == 'Total must be at least':
                raise PoloniexInvalidQuantity()

            elif err == 'Unable to fill order completely.':
                raise PoloniexUnableToFill()

            elif err == 'Invalid currency pair.':
                raise PoloniexInvalidCurrencyPair()

            elif err == 'Order not found, or you are not the person who placed it.':
                raise PoloniexOrderNotFound()

            elif err == 'Invalid order number, or you are not the person who placed the order.':
                raise PoloniexOrderNotFound()

            elif err == 'Permission denied.':
                raise PoloniexPermissionDenied()

            elif err == 'Please do not make more than 6 API calls per second.':
                raise PoloniexRateLimiting()

            else:
                raise PoloniexUnknown(err)

    def _get(self, path, **kwargs):
        url = self._build_url(path)
        res = requests.get(url, params=kwargs)
        res_json = res.json()
        self._check_errors(res_json)
        return res_json

    def _trade_post(self, path, **kwargs):
        kwargs['nonce'] = int(time.time() * 1000000)
        data = urlencode(kwargs).encode('utf-8')
        sign = hmac.new(self._secret, data, hashlib.sha512).hexdigest()
        headers = {
            'Key': self._key,
            'Sign': sign,
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        }
        url = self._build_url(path)
        res = requests.post(url, headers=headers, data=data)
        try:
            res_json = res.json()
        except:
            raise PoloniexFailedToParseResponse()
        self._check_errors(res_json)
        return res_json

    def get_orders(self, currency_pair='all', depth=10):
        res = self._get('/public', command='returnOrderBook', currencyPair=currency_pair, depth=depth)

        if currency_pair == 'all':
            return res
        else:
            return {currency_pair: res}

    def get_tickers(self):
        return self._get('/public', command='returnTicker')

    def get_balances(self):
        return self._trade_post('/tradingApi', command='returnBalances')

    def get_order_trades(self, order_number):
        return self._trade_post('/tradingApi', command='returnOrderTrades', orderNumber=order_number)

    def move_order(self, order_number, price, **kwargs):
        """
        kwargs:
        - amount: change the amount of the order
        - post_only: maker fees
        """
        kwargs['command'] = 'moveOrder'
        kwargs['orderNumber'] = order_number
        kwargs['rate'] = price
        return self._trade_post('/tradingApi', **kwargs)

    def cancel_order(self, order_number):
        kwargs = {}
        kwargs['orderNumber'] = order_number
        kwargs['command'] = 'cancelOrder'
        return self._trade_post('/tradingApi', **kwargs)

    def get_open_orders(self, security):
        return self._trade_post('/tradingApi', command='returnOpenOrders', currencyPair=security)

    def _prep_buy_sell_params(self, currency_pair, rate, amount, **kwargs):
        params = {
            'currencyPair': currency_pair,
            'rate': rate,
            'amount': amount
        }
        if kwargs.get('fill_or_kill', False):
            params['fillOrKill'] = 1
        elif kwargs.get('immediate_or_cancel', False):
            params['immediateOrCancel'] = 1
        elif kwargs.get('post_only', False):
            params['postOnly'] = 1
        return params

    def buy(self, currency_pair, rate, amount, **kwargs):
        params = self._prep_buy_sell_params(currency_pair, rate, amount, **kwargs)
        params['command'] = 'buy'
        return self._trade_post('/tradingApi', **params)

    def sell(self, currency_pair, rate, amount, **kwargs):
        params = self._prep_buy_sell_params(currency_pair, rate, amount, **kwargs)
        params['command'] = 'sell'
        return self._trade_post('/tradingApi', **params)


class PoloniexMockException(Exception):
    pass


class PoloniexMock:
    res_list = {}
    sell = Mock()
    buy = Mock()
    open_orders = Mock()
    order_book = Mock()
    order_trades = Mock()
    move_order = Mock()
    cancel_order = Mock()

    INVALID_SECURITY = 'Invalid currency pair.'
    INVALID_QUANTITY = 'Total must be at least 0.0001.'
    UNABLE_TO_FILL = 'Unable to fill order completely.'
    UNKNOWN_ERROR = 'Unknown error.'
    ORDER_NOT_FOUND = 'Order not found, or you are not the person who placed it.'

    @classmethod
    def reset(cls):
        cls.res_list = {}
        cls.sell = Mock()
        cls.buy = Mock()
        cls.open_orders = Mock()
        cls.order_book = Mock()
        cls.order_trades = Mock()
        cls.move_order = Mock()
        cls.cancel_order = Mock()

    @classmethod
    def _get(cls, method):
        if (method not in cls.res_list
            or len(cls.res_list[method]) == 0):
            msg = 'No responses registered for method `{}`'.format(method)
            raise PoloniexMockException(msg)

        return cls.res_list[method].pop()

    @classmethod
    def _return_order_book(cls, req):
        cls.order_book(req)
        return cls._get('order_book')

    @classmethod
    def _return_order_trades(cls, req):
        cls.order_trades(req)
        return cls._get('order_trades')

    @classmethod
    def _move_order(cls, req):
        cls.move_order(req)
        return cls._get('move_order')

    @classmethod
    def _cancel_order(cls, req):
        cls.cancel_order(req)
        return cls._get('cancel_order')

    @classmethod
    def _sell(cls, req):
        cls.sell(req)
        return cls._get('sell')

    @classmethod
    def _buy(cls, req):
        cls.buy(req)
        return cls._get('buy')

    @classmethod
    def _add(cls, method, **kwargs):
        status = kwargs.get('status', 200)
        headers = kwargs.get('headers', {})
        body = kwargs.get('body', {})
        exception = kwargs.get('exception', None)

        if exception is not None:
            body = {
                'error': exception
            }

        if method not in cls.res_list:
            cls.res_list[method] = []
        cls.res_list[method].insert(0, (status, headers, body,))

    @classmethod
    def _return_open_orders(cls, req):
        cls.open_orders(req)
        return cls._get('open_orders')

    @classmethod
    def add_sell(cls, **kwargs):
        cls._add('sell', **kwargs)

    @classmethod
    def add_buy(cls, **kwargs):
        cls._add('buy', **kwargs)

    @classmethod
    def add_order_book(cls, **kwargs):
        cls._add('order_book', **kwargs)

    @classmethod
    def add_order_trades(cls, **kwargs):
        cls._add('order_trades', **kwargs)

    @classmethod
    def add_open_orders(cls, **kwargs):
        cls._add('open_orders', **kwargs)

    @classmethod
    def add_move_order(cls, **kwargs):
        cls._add('move_order', **kwargs)

    @classmethod
    def add_cancel_order(cls, **kwargs):
        cls._add('cancel_order', **kwargs)

    @classmethod
    def _public_api(cls, req):
        command = re.search('command=([A-Za-z]*)', req.path_url).group(1)

        if command == 'returnOrderBook':
            (status_code, headers, body_dict,) = cls._return_order_book(req)
        else:
            raise PoloniexMockException('Public command `{}` not handled'.format(command))

        return (status_code, headers, json.dumps(body_dict),)

    @classmethod
    def _trade_api(cls, req):
        body = parse_qs(req.body.decode('utf-8'))
        command = body['command'][0]

        if command == 'sell':
            (status_code, headers, body_dict,) = cls._sell(req)
        elif command == 'buy':
            (status_code, headers, body_dict,) = cls._buy(req)
        elif command == 'returnOpenOrders':
            (status_code, headers, body_dict,) = cls._return_open_orders(req)
        elif command == 'returnOrderTrades':
            (status_code, headers, body_dict,) = cls._return_order_trades(req)
        elif command == 'moveOrder':
            (status_code, headers, body_dict,) = cls._move_order(req)
        elif command == 'cancelOrder':
            (status_code, headers, body_dict,) = cls._cancel_order(req)
        else:
            raise PoloniexMockException('Trade command `{}` not handled'.format(command))

        return (status_code, headers, json.dumps(body_dict),)

    @classmethod
    def activate(cls):
        def test_decorator(old_function):
            requests_mock = RequestsMock(assert_all_requests_are_fired=False)
            test_decorated = requests_mock.activate(old_function)

            requests_mock.add_callback(
                requests_mock.GET,
                'https://poloniex.com/public',
                callback=PoloniexMock._public_api, 
                content_type='application/json'
            )

            requests_mock.add_callback(
                requests_mock.POST,
                'https://poloniex.com/tradingApi',
                callback=PoloniexMock._trade_api,
                content_type='application/json'
            )

            return test_decorated
        return test_decorator