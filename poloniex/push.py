import asyncio
from autobahn.asyncio.wamp import ApplicationSession, ApplicationRunner


class PoloniexPush:
    """
    Wrapper around the poloniex push API; used for collectors

    Usage:

    """

    class Component(ApplicationSession):

        async def onJoin(self, details):
            callbacks = self.config.extra['callbacks']
            loop = self.config.extra['loop']

            for event, callback in callbacks.items():
                await self.subscribe(callback, event)

        def onDisconnect(self):
            print('Disconnecting')


    def __init__(self, loop, callbacks):
        """
        `loop` should be the event loop
        `callbacks` should be a dictionary where the keys are events to
        subscribe to and the values are callbacks to invoke for the methods
        """

        extras = {
            'callbacks': callbacks,
            'loop': loop
        }

        self._runner = ApplicationRunner(url=u'wss://api.poloniex.com', realm=u'realm1',
                                         extra=extras)

    def run(self):
        self._runner.run(PoloniexPush.Component)


class PoloniexOrderBook:
    """
    Creates an order book for the provided currencies.  Each
    callback should correspond to a currency pair.
    """
    def __init__(self, loop, callbacks, **kwargs):
        self._api = Poloniex('', '')
        wrapped_callbacks = {}

        for security, callback in callbacks.items():
            data = {
                'order_book': {
                    'bid': {},
                    'ask': {}
                },
                'security': security,
                'updates_since_sync': None
            }
            wrapped_callbacks[security] = self._wrap_callback(data, callback)


        self._push = PoloniexPush(loop, wrapped_callbacks)

    def run(self):
        self._push.run()

    def _wrap_callback(self, data, callback):
        def apply_updates(order_book, updates):
            trades = []

            for update in updates:
                data = update['data']
                t = update['type']

                if t == 'orderBookModify':
                    order_book[data['type']][data['rate']] = data['amount']

                elif t == 'orderBookRemove':
                    if data['rate'] in order_book[data['type']]:
                        del order_book[data['type']][data['rate']]

                elif t == 'newTrade':
                    trades.append(update['data'])

            return trades

        def on_data(*updates, **kwargs):
            updates_since_sync = data['updates_since_sync']

            if updates_since_sync is None or updates_since_sync >= 1000:
                try:
                    res = self._api.get_orders(data['security'], depth=500)[data['security']]
                    asks = {}
                    bids = {}

                    for rate, amount in res['asks']:
                        asks[rate] = amount

                    for rate, amount in res['bids']:
                        bids[rate] = amount

                    data['order_book'] = {
                        'ask': asks,
                        'bid': bids
                    }

                    data['updates_since_sync'] = 0
                    data['sync_seq'] = res['seq']
                except PoloniexRateLimiting:
                    pass

            order_book = data['order_book']
            seq = kwargs.get('seq')

            if seq > data['sync_seq']:
                data['updates_since_sync'] += 1
                trades = apply_updates(order_book, updates)
            else:
                trades = []

            callback(data['order_book'], trades)

        return on_data