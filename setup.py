from distutils.core import setup
from setuptools.command.install import install
import pip
import os



class CustomInstall(install):
  def run(self):
    install.run(self)
    wheel_files = os.listdir('dist')
    for wheel_file in wheel_files:
      full_path = 'dist/{}'.format(wheel_file)
      pip.main(['install', full_path])


setup(
  name = 'poloniex',
  py_modules = ['poloniex.http', 'poloniex.push'],
  version = '1.8',
  description = 'A random test lib',
  author = 'Connor Bode',
  author_email = 'connor@matix.io',
  url = 'https://gitlab.com/matix-imperio/poloniex-api',
  download_url = 'https://gitlab.com/matix-imperio/poloniex-api',
  keywords = ['testing', 'logging', 'example'],
  classifiers = [],
  install_requires = [
    'autobahn',
    'requests',
    'responses'
  ],
  # cmdclass = {
  #   'install': CustomInstall
  # }
)