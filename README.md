# Installation

`pip install git+ssh://git@gitlab.com/matix-io/poloniex-api.git`


# Usage

For HTTP API:

```python
from poloniex.http import Poloniex


# init like this
API_KEY = 'poloniex-api-key'
API_SECRET = bytes('poloniex-api-secret'.encode('utf-8'))
api = Poloniex(API_KEY, API_SECRET)

# make calls like this
res = api.get_orders()
res = api.get_balances()
``` 

For Push API:

```python
import asyncio
from poloniex.push import PoloniexPush


class Wrapper:
  def __init__(self, loop):
    callbacks = {
      'event_name': self._event_handler
    }
    self.push = PoloniexPush(loop, callbacks)

  def run(self):
    self.push.run()

  def _event_handler(self, *args):
    pass

w = Wrapper()
w.run()
```


# Building

Installation is done via Wheels.  To build the wheels, just run `make`.  Wheels are only being built for OSX at the moment (I think, not sure how this works).